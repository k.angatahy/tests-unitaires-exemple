#include "multiplication_test.h"
#include "../src/arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(multiplicationTest);

void multiplicationTest::setUp() {}

void multiplicationTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * multiplicationTest
 */
void multiplicationTest::multiplication_normal(){
  operandeA = 5;
  operandeB = 4;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(20),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );
  CPPUNIT_ASSERT(arithmetique::multiplication(operandeA, operandeB) != 25);
}

void multiplicationTest::multiplication_negatif(){

  operandeA = -5;
  operandeB = 4;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(-20),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );

  CPPUNIT_ASSERT(arithmetique::multiplication(operandeA, operandeB) <= 0);

  operandeB = -4;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(20),
                        static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );

                 CPPUNIT_ASSERT(arithmetique::multiplication(operandeA, operandeB) >= 0);
}

void multiplicationTest::multiplication_zero(){

  operandeA = 10;
  operandeB = 0;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );

  CPPUNIT_ASSERT(arithmetique::multiplication(operandeA, operandeB) == 0);

  operandeA = 0;
  operandeB = 10;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );

  CPPUNIT_ASSERT(arithmetique::multiplication(operandeA, operandeB) == 0);

}

void multiplicationTest::multiplication_un(){

  operandeA = 1;
  operandeB = 25;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(25), static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))

    );

  operandeA = 25;
  operandeB = 1;

  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(25), static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );   }
